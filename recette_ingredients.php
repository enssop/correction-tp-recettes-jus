<?php

require 'includes/header.php';

if ( isset($_POST['ajout_ingredient'])) {
    $portions = $_POST['nb_portions'];

    $ingredient_exist = $dbh->prepare("SELECT * FROM ingredients_recettes WHERE
        id_recettes = :id_recettes
        AND id_ingredients = :id_ingredients
    ");

    $ingredient_exist->execute([
        'id_ingredients' => $_POST['id_ingredient'],
        'id_recettes' => $_GET['id'],
    ]);

    if ( $ingredient_exist->rowCount() > 0 ) {
        $ingredient_exist = $ingredient_exist->fetch();
        $portions += $ingredient_exist['nb_portions'];
        $deleteOld = $dbh->prepare("DELETE FROM ingredients_recettes WHERE
        id_recettes = :id_recettes
        AND id_ingredients = :id_ingredients");
        $deleteOld->execute([
            'id_ingredients' => $_POST['id_ingredient'],
            'id_recettes' => $_GET['id'],
        ]);

    }

    $ajoutIngredient = $dbh->prepare("INSERT INTO ingredients_recettes 
    (id_ingredients, id_recettes, nb_portions) VALUES 
    (:id_ingredients, :id_recettes, :nb_portions)");

    $ajoutIngredient->execute([
        'id_ingredients' => $_POST['id_ingredient'],
        'id_recettes' => $_GET['id'],
        'nb_portions' => $portions,
    ]);

}

?><!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ingrédients d'une recette</title>
</head>
<body>

<?php
$recette = $dbh->prepare("SELECT * FROM recettes WHERE id_recettes = :id");
$recette->execute([
    'id' => $_GET['id'],
]);
// on vérifie si il y a bien une recette
if ($recette->rowCount() < 1) exit;

$recette = $recette->fetch();

echo '<h1>' . $recette['nom'] . '</h1>';

$ingredients = $dbh->prepare("
    SELECT *
    
    FROM ingredients
        
        INNER JOIN ingredients_recettes 
            ON ingredients.id_ingredients = ingredients_recettes.id_ingredients
    
    WHERE ingredients_recettes.id_recettes = :id_recette
");
$ingredients->execute([
    'id_recette' => $recette['id_recettes'],
]);

echo '<h2>ingredients</h2>';

while ($ingredient = $ingredients->fetch()) {
    echo $ingredient['nom'].'*'.$ingredient['nb_portions'].'<br>';
}

echo '<h2>Ajouter un ingrédient</h2>';
?>
<form action="recette_ingredients.php?id=<?php echo $recette['id_recettes']; ?>" method="post">

    ingredient : <select name="id_ingredient">
        <?php
        $ingredientsDisponibles =  $dbh->prepare("SELECT * FROM ingredients");
        $ingredientsDisponibles->execute();
        while ( $ingredientsDisponible = $ingredientsDisponibles->fetch()) {
            echo '<option value="'.$ingredientsDisponible['id_ingredients'].'">'
                .$ingredientsDisponible['nom'].'</option>';
        }
        ?>
    </select><br>
    qte : <input type="number" name="nb_portions"><br>


    <input type="submit" value="Ajouter l'ingrédient" name="ajout_ingredient">

</form>


</body>
</html>