<?php

require 'includes/header.php';

if (isset($_POST['ajout_recette'])) {
    $ajoutRecette = $dbh->prepare("INSERT INTO recettes 
    (nom, descriptions, photo) 
    VALUES (:nom, :descriptions, '')");

    $ajoutRecette->execute([
        'nom' => $_POST['nom'] ? $_POST['nom'] : '',
        'descriptions' => $_POST['descriptions'] ? $_POST['descriptions'] : '',
    ]);
}

?><!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recettes</title>
</head>
<body>

<h2>Liste recettes</h2>
<?php
$recettes = $dbh->prepare("SELECT * FROM recettes");
$recettes->execute();
while ($recette = $recettes->fetch()) {
    echo $recette['nom'] . ' <a href="recette_ingredients.php?id='.$recette['id_recettes'].'">ingrédients</a><br>';
}
?>

<h2>Ajouter une recette</h2>
<form action="recette.php" method="post">
    nom: <input type="text" name="nom"><br>
    description: <textarea name="descriptions"></textarea><br>
    <input type="submit" value="Ajouter la recette" name="ajout_recette">
</form>

</body>
</html>
