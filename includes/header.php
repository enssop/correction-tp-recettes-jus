<?php

require "config.php";

try {
    $dbh = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
} catch (PDOException $e) {
    //var_dump($e);
    echo "Impossible de se connecter à la base de données";
    exit;
}
