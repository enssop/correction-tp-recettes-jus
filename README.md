# TP Plateforme de partage de recettes de jus

Correction du TP du 9 mars 2020.

## Installation de la structure de la base de donnée

La base de données porte le nom **jusfruits** et doit être créer manuellement. 

La structure de la base de données se trouve dans le fichier database.sql

Saisir une des commandes suivantes dans un terminal pour executer les requetes présentes dans le fichier database.sql :

``mysql < database.sql``

pour spécifier un nom d'utilisateur et un mot de passe : 

``mysql -u nomdutilisateur -p < database.sql``

## Exemple de fichier de configuration

le fichier doit s'appeller ``config.php`` et contenir :
```
<?php

$db_host = "";
$db_name = "";
$db_user = "";
$db_pass = "";
``` 
