<?php

require "includes/header.php";

if (isset($_POST['label'])) {
    $addType = $dbh->prepare("INSERT INTO types (label) VALUES (:label)");
    $addType->execute([
        'label' => $_POST['label'],
    ]);
    header("Location: type_ingredient.php");
}

if (isset($_GET['sid'])) {
    $deleteType = $dbh->prepare("DELETE FROM types WHERE id_types = :id");
    $deleteType->execute([
        'id' => $_GET['sid'],
    ]);
    header("Location: type_ingredient.php");
}

?><!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Types d'ingrédients</title>
</head>
<body>

<h2>Liste des types d'ingrédients</h2>
<?php

$types = $dbh->prepare("SELECT * FROM types ORDER BY label");
$types->execute();

while ($type = $types->fetch()) {
    echo $type['label'] . '<a href="type_ingredient.php?sid=' . $type['id_types'] . '">X</a><br>';
}

?>

<h2>Ajouter un type d'ingrédient</h2>
<form action="type_ingredient.php" method="post">
    <label>Type d'ingrédient : <input type="text" name="label"></label><br>
    <input type="submit" value="Ajouter le type d'ingrédient">
</form>

</body>
</html>
