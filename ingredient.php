<?php

require "includes/header.php";

// modification d'un ingrédient
if (isset($_POST['modifier_ingredient'])) {

    // on prépare la requête
    $editIngredient = $dbh->prepare("UPDATE ingredients SET
            nom = :nom,
            descriptions = :descriptions,
            id_types = :id_types
            WHERE 
            id_ingredients = :id_ingredients
        ");

    // on execute la requête avec ses paramètres
    // @todo: vérifier la cohérence des paramètres
    $editIngredient->execute([
        'id_types' => $_POST['type'],
        'nom' => $_POST['nom'] ? $_POST['nom'] : '',
        'descriptions' => $_POST['description'] ? $_POST['description'] : '',
        'id_ingredients' => $_POST['id_ingredients'],
    ]);

    if ( isset($_FILES['photo']) && !empty($_FILES['photo']['tmp_name']) ) {
        $fileName = "uploads/" . str_replace([" ", "."], "", microtime()) . $_FILES['photo']['name'];
        move_uploaded_file($_FILES['photo']['tmp_name'], $fileName);

        // on prépare la requête pour la mise à jour de la photo
        $editIngredient = $dbh->prepare("UPDATE ingredients SET
            photo = :photo
            WHERE 
            id_ingredients = :id_ingredients
        ");

        // on execute la requête avec ses paramètres
        $editIngredient->execute([
            'photo' => $fileName,
            'id_ingredients' => $_POST['id_ingredients'],
        ]);
    }

    header("Location: ingredient.php");

}

// ajout d'un ingrédient
if (isset($_POST['ajout_ingredient'])) {

    // on prépare la requête
    $addIngredient = $dbh->prepare("INSERT INTO ingredients 
    (id_types, nom, descriptions, photo) 
    VALUES (:id_types, :nom, :descriptions, :photo)");

    $fileName = "";
    if ( isset($_FILES['photo']) && !empty($_FILES['photo']['tmp_name']) ) {
        $fileName = "uploads/".str_replace([" ", "."], "", microtime()).$_FILES['photo']['name'];
        move_uploaded_file($_FILES['photo']['tmp_name'], $fileName);
    }

    // on execute la requête avec ses paramètres
    // @todo: vérifier la cohérence des paramètres
    $addIngredient->execute([
        'id_types' => $_POST['type'],
        'nom' => $_POST['nom'] ? $_POST['nom'] : '',
        'descriptions' => $_POST['description'] ? $_POST['description'] : '',
        'photo' => $fileName,
    ]);

    header("Location: ingredient.php");

}

// suppression d'un ingrédient
if (isset($_GET['sid'])) {
    $deleteIngredient = $dbh->prepare("DELETE FROM ingredients WHERE id_ingredients = :id");
    $deleteIngredient->execute([
        'id' => $_GET['sid'],
    ]);

    header("Location: ingredient.php");
    
}

?><!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des ingrédients</title>
</head>
<body>

<h2>Liste des ingrédients</h2>
<?php
$ingredients = $dbh->prepare("SELECT * FROM ingredients");
$ingredients->execute();
while ($ingredient = $ingredients->fetch()) {
    echo '<img src="'.$ingredient['photo'].'" style="width:100px;">';
    echo $ingredient['nom'] . ' <a href="ingredient.php?eid=' . $ingredient['id_ingredients'] . '">modifier</a> <a href="ingredient.php?sid=' . $ingredient['id_ingredients'] . '">supprimer</a><br>';
}
?>

<?php
if ( isset($_GET['eid']) ) {
    $ingredient = $dbh->prepare("SELECT * FROM ingredients WHERE id_ingredients = :id");
    $ingredient->execute([
        'id' => $_GET['eid'],
    ]);
    $ingredient = $ingredient->fetch();

    //var_dump($ingredient);

} else {
    $ingredient = null;
}


echo $ingredient ? '<h2>Modifier un ingrédient</h2>' : '<h2>Ajouter un ingrédient</h2>';
?>
<form action="ingredient.php" method="post" enctype="multipart/form-data">
    <?php
    if ( $ingredient ) {
        echo '<input type="hidden" name="id_ingredients" value="'.$ingredient['id_ingredients'].'">';
    }
    ?>
    Nom: <input type="text" name="nom" value="<?php echo $ingredient ? $ingredient['nom'] : '' ?>"><br>
    Description : <textarea name="description" value="<?php echo $ingredient ? $ingredient['descriptions'] : '' ?>"></textarea><br>
    Photo : <input type="file" name="photo"><br>
    Type : <select name="type">
        <?php
        $types = $dbh->prepare("SELECT * FROM types");
        $types->execute();
        while ($type = $types->fetch()) {
            echo '<option value="' . $type['id_types'] . '" '.
                ( $type['id_types'] == $ingredient['id_types'] ? 'selected': '' )
                .'>' . $type['label'] . '</option>';
        }
        ?>
    </select>
    <?php
    echo $ingredient ? '<input type="submit" value="Modifier l\'ingrédient" name="modifier_ingredient">'
        : '<input type="submit" value="Ajouter un ingrédient" name="ajout_ingredient">';
    ?>

</form>

</body>
</html>
