USE jusfruits;

CREATE TABLE administrateur
(
    id_admin INT         NOT NULL AUTO_INCREMENT,
    nom      VARCHAR(80) NOT NULL,
    mdp      VARCHAR(80) NOT NULL,
    PRIMARY KEY (id_admin)
);

CREATE TABLE recettes
(
    id_recettes  INT         NOT NULL AUTO_INCREMENT,
    nom          VARCHAR(80) NOT NULL,
    descriptions TEXT        NOT NULL,
    photo        VARCHAR(255)     NOT NULL,
    PRIMARY KEY (id_recettes)
);

CREATE TABLE types
(
    id_types INT     NOT NULL AUTO_INCREMENT,
    label  VARCHAR(255) NOT NULL,
    PRIMARY KEY (id_types)
);

CREATE TABLE ingredients
(
    id_ingredients INT         NOT NULL AUTO_INCREMENT,
    id_types       INT         NOT NULL,
    nom            VARCHAR(80) NOT NULL,
    descriptions   TEXT        NOT NULL,
    photo          VARCHAR(255)     NOT NULL,
    PRIMARY KEY (id_ingredients),
    FOREIGN KEY (id_types) REFERENCES types (id_types)
);

CREATE TABLE ingredients_recettes
(
    id_ingredients INT NOT NULL,
    id_recettes    INT NOT NULL,
    nb_portions    INT NOT NULL,
    FOREIGN KEY (id_ingredients) REFERENCES ingredients (id_ingredients),
    FOREIGN KEY (id_recettes) REFERENCES recettes (id_recettes)
);
